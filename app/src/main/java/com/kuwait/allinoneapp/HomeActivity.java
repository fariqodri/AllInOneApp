package com.kuwait.allinoneapp;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.GridView;

public class HomeActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate( savedInstanceState );
        setContentView( R.layout.activity_home );
        GridView gv = findViewById( R.id.gridview );
        final int[] images = {R.mipmap.browser, R.mipmap.forms};
        CharSequence[] names ={getText( R.string.calculator ), getText( R.string.notes )};
        gv.setAdapter( new AppAdapter(this, images, names ));
        gv.setOnItemClickListener( new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                switch (i) {
                    case 0:
                        startActivity( new Intent( getApplicationContext(), CalculatorActivity.class ) );
                        break;
                    case 1:
                        startActivity( new Intent(getApplicationContext(), NotesDashboardActivity.class) );
                        break;
                }
            }
        } );
    }
}
