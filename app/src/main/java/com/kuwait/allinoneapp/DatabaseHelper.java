package com.kuwait.allinoneapp;

import android.app.Notification;
import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.os.Message;
import android.util.Log;
import android.widget.Toast;

/**
 * Created by pc on 2/1/2018.
 */

public class DatabaseHelper extends SQLiteOpenHelper {
    private static final String DATABASE_NAME = "NOTES_DB";    // Database Name
    public static final String TABLE_NAME = "NOTES";   // Table Name
    private static final int DATABASE_Version = 1;   // Database Version
    public static final String UID="_id";     // Column I (Primary Key)
    public static final String TITLE = "Title";    //Column II
    public static final String NOTE = "Note";    // Column III
    private static final String CREATE_TABLE = "CREATE TABLE "+TABLE_NAME+
            " ("+UID+" INTEGER PRIMARY KEY AUTOINCREMENT, "+TITLE+" VARCHAR(255) ,"+ NOTE+" VARCHAR(225));";
    private static final String DROP_TABLE ="DROP TABLE IF EXISTS "+TABLE_NAME;
    private Context context;

    public DatabaseHelper(Context context) {
        super( context, DATABASE_NAME, null, DATABASE_Version);
        this.context = context;
    }

    @Override
    public void onCreate(SQLiteDatabase sqLiteDatabase) {
        try{
            sqLiteDatabase.execSQL( CREATE_TABLE );
        }
        catch (Exception e) {
            Toast.makeText( context, e+"", Toast.LENGTH_LONG ).show();
        }
    }

    @Override
    public void onUpgrade(SQLiteDatabase sqLiteDatabase, int i, int i1) {
        try {
            Toast.makeText( context, "OnUpgrade", Toast.LENGTH_LONG ).show();
            sqLiteDatabase.execSQL( DROP_TABLE );
            onCreate( sqLiteDatabase );
        }
        catch (Exception e) {
            Toast.makeText( context, e+"", Toast.LENGTH_LONG ).show();
        }
    }
}
