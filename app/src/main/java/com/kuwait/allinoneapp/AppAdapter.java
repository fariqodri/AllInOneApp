package com.kuwait.allinoneapp;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

/**
 * Created by pc on 1/31/2018.
 */

public class AppAdapter extends BaseAdapter {
    Context context;
    int[] images;
    CharSequence[] names;

    public AppAdapter(Context kons, int[] picts, CharSequence[] names) {
        this.context = kons;
        this.images = picts;
        this.names = names;
    }
    @Override
    public int getCount() {
        return images.length;
    }

    @Override
    public Object getItem(int i) {
        return null;
    }

    @Override
    public long getItemId(int i) {
        return 0;
    }

    @Override
    public View getView(int i, View view, ViewGroup viewGroup) {
        final int image = images[i];
        final CharSequence name= names[i];
        if(view == null) {
            final LayoutInflater layoutInflater =LayoutInflater.from( context );
            view = layoutInflater.inflate( R.layout.app_layout, null );
        }
        final ImageView imageView = view.findViewById( R.id.app_icon );
        TextView textView = view.findViewById( R.id.app_name );
        imageView.setImageResource( image );
        textView.setText( name );
        return view;
    }
}
