package com.kuwait.allinoneapp;

import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/**
 * Created by pc on 2/1/2018.
 */

public class NotesAdapter extends RecyclerView.Adapter<NotesAdapter.MyViewHolder> {
    String[] content;

    public NotesAdapter(String[] content) {
        this.content = content;
    }

    public static class MyViewHolder extends RecyclerView.ViewHolder {
        TextView title;
        TextView note;

        public MyViewHolder(View itemView) {
            super(itemView);
            title = itemView.findViewById(R.id.titledash);
            note = itemView.findViewById(R.id.notedash);
        }
    }

    @Override
    public NotesAdapter.MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.card_view, parent, false);
        return new MyViewHolder(v);
    }

    @Override
    public int getItemCount() {
        return content.length;
    }

    @Override
    public void onBindViewHolder(MyViewHolder holder, int position) {
        String title = content[content.length - 1 - position].split( "/" )[1];
        String note = content[content.length - 1 - position].split( "/" )[2];
        holder.title.setText(title);
        if(note.length() > 100) {
            holder.note.setText(note.substring(0, 100));
        }
        else {
            holder.note.setText(note);;
        }
    }

}
