package com.kuwait.allinoneapp;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.widget.EditText;
import android.widget.Toast;

public class MakeNotesActivity extends AppCompatActivity {
    EditText title;
    EditText note;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate( savedInstanceState );
        setContentView( R.layout.activity_make_notes );

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.app_bar, menu);
        return true;
    }


    private void saveToDatabase() {
        DatabaseAdapter db = new DatabaseAdapter( this );
        title =  findViewById( R.id.title );
        note = findViewById( R.id.note );
        String titleString = title.getText().toString();
        String noteString = note.getText().toString();
        if(titleString.equals( "" ) || noteString.equals( "" )) {
            Toast.makeText(this, "Your note is empty", Toast.LENGTH_SHORT);
        }
        else {
            db.insertData( title.getText().toString(), note.getText().toString() );
        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.action_clear :
                title.setText( "" );
                note.setText( "" );
                break;

            case R.id.action_save :
                saveToDatabase();
                Intent i = new Intent( getBaseContext(), NotesDashboardActivity.class );
                i.addFlags( Intent.FLAG_ACTIVITY_SINGLE_TOP );
                startActivity( i );
                break;
        }
        return super.onOptionsItemSelected( item );
    }
}
