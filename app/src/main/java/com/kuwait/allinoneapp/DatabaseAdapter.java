package com.kuwait.allinoneapp;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;

/**
 * Created by pc on 2/1/2018.
 */

public class DatabaseAdapter {
    DatabaseHelper dbHelper;

    public DatabaseAdapter(Context context) {
        dbHelper = new DatabaseHelper( context );
    }

    public long insertData(String title, String note)
    {
        SQLiteDatabase dbb = dbHelper.getWritableDatabase();
        ContentValues contentValues = new ContentValues();
        contentValues.put(DatabaseHelper.TITLE, title);
        contentValues.put(DatabaseHelper.NOTE, note);
        long id = dbb.insert(DatabaseHelper.TABLE_NAME, null , contentValues);
        return id;
    }

    public String getData()
    {
        SQLiteDatabase db = dbHelper.getWritableDatabase();
        String[] columns = {DatabaseHelper.UID,DatabaseHelper.TITLE,DatabaseHelper.NOTE};
        Cursor cursor =db.query(DatabaseHelper.TABLE_NAME,columns,null,null,null,null,null);
        StringBuffer buffer= new StringBuffer();
        while (cursor.moveToNext())
        {
            int cid =cursor.getInt(cursor.getColumnIndex(DatabaseHelper.UID));
            String title =cursor.getString(cursor.getColumnIndex(DatabaseHelper.TITLE));
            String  note =cursor.getString(cursor.getColumnIndex(DatabaseHelper.NOTE));
            if(title == null){
                buffer.append(cid+ "/" + note +"\n");
            }
            else if(note == null){
                buffer.append(cid+ "/" + title + "\n");
            }
            else {
                buffer.append(cid+ "/" + title + "/" + note +"\n");
            }
        }
        return buffer.toString();
    }


    public int delete(String uname)
    {
        SQLiteDatabase db = dbHelper.getWritableDatabase();
        String[] whereArgs ={uname};
        int count =db.delete(DatabaseHelper.TABLE_NAME ,DatabaseHelper.TITLE+" = ?",whereArgs);
        return count;
    }

//    public int update(String oldTitle, String newTitle) {
//        SQLiteDatabase db = dbHelper.getWritableDatabase();
//        ContentValues contentValues = new ContentValues(  );
//        contentValues.put( DatabaseHelper.TITLE, newTitle );
//    }
}
