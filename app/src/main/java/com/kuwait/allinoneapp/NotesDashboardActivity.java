package com.kuwait.allinoneapp;

import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.CardView;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.support.v7.widget.helper.ItemTouchHelper;
import android.view.View;
import android.widget.ListView;

import java.util.Arrays;

public class NotesDashboardActivity extends AppCompatActivity {


    //TODO: Implement back to home after creating note, swipe to delete, edit note
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate( savedInstanceState );
        setContentView( R.layout.activity_notes_dashboard );
        Toolbar toolbar = (Toolbar) findViewById( R.id.toolbar );
        setSupportActionBar( toolbar );
        RecyclerView myView =  (RecyclerView)findViewById(R.id.my_recycler_view);
        final DatabaseAdapter db = new DatabaseAdapter(this);
        String data = db.getData();
        String[] content;
        if(data != null){ content = data.split( "\n" );}
        else {content = new String[0];}
        NotesAdapter adapter = new NotesAdapter(content);
        myView.setHasFixedSize(true);
        myView.setAdapter(adapter);
        LinearLayoutManager llm = new LinearLayoutManager(this);
        llm.setOrientation(LinearLayoutManager.VERTICAL);
        myView.setLayoutManager(llm);
        FloatingActionButton fab = (FloatingActionButton) findViewById( R.id.fab );
        fab.setOnClickListener( new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent i = new Intent( getBaseContext(), MakeNotesActivity.class );
                i.addFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP);
                startActivity( i );
            }
        } );
    }
}
